set -x
#stat --format '%N %s %f %Y' commits/dc0e339816b751ba3a404e5301c4646a3438f3bd/child
#stat --format '%N %s %f %Y' commits/dc0e339816b751ba3a404e5301c4646a3438f3bd/child/parent
stat --format '%N %s %f %Y' commits/dc0e339816b751ba3a404e5301c4646a3438f3bd/children
stat --format '%N %s %f %Y' commits/dc0e339816b751ba3a404e5301c4646a3438f3bd/children/*

# error checks
stat commits/dc0e339816b751ba3a404e5301c4646a3438f3bd/children/not_existing
