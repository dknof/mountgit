set -x
cd commits/8ae619bc794fdad0cb34de138b95f8c9d9ec7dcd/
ls
cat author
cat committer
cat summary
cat message
cat body
#cat time # output is in local time

# error checking
stat not_existing
cat not_existing
stat not_existing/1
