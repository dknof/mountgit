set -x
ls commits-by-day/2019/
ls commits-by-day/2019/09/
ls commits-by-day/2019/09/08/
cat commits-by-day/2019/09/08/44aa1e68194596a040e8a5b0cc531411774146e8/message

# references to same day
pushd commits-by-day/2019/09/08/44aa1e68194596a040e8a5b0cc531411774146e8/ >/dev/null
stat --format '%N %s %f %Y' parents/e24b4a3df3fde6549afb1b5fea9bb77e9a74e378
ls parents/e24b4a3df3fde6549afb1b5fea9bb77e9a74e378/id
stat --format '%N %s %f %Y' parent
cat parent/id
stat --format '%N %s %f %Y' children/6a00fce836a334813663473297c4132bfc6863eb
cat children/6a00fce836a334813663473297c4132bfc6863eb/id
popd >/dev/null

# references to another day
pushd commits-by-day/2019/09/24/9f595c81ba3ad658100dee07e28dbe04f832d218/ >/dev/null
stat --format '%N %s %f %Y' parents/6434885168d52d1ed01c2e5215fe433decaab609
cat parents/6434885168d52d1ed01c2e5215fe433decaab609/id
stat --format '%N %s %f %Y' parent
cat parent/id
stat --format '%N %s %f %Y' children/b89b2de8982cace1887baf1ec5b2b2929eb5bb8e
cat children/b89b2de8982cace1887baf1ec5b2b2929eb5bb8e/id
popd >/dev/null

# references to another month
pushd commits-by-day/2019/10/03/93ac371b16c247551cff974f9a29ffcc93ddc432/ >/dev/null
stat --format '%N %s %f %Y' parents/*
cat parents/c4aab570b7b58aa8243a567b02fcc6df957a1240/id
stat --format '%N %s %f %Y' parent
cat parent/id
popd >/dev/null
stat --format '%N %s %f %Y' commits-by-day/2019/09/30/c4aab570b7b58aa8243a567b02fcc6df957a1240/children/93ac371b16c247551cff974f9a29ffcc93ddc432/id

# error checks
stat commits-by-day/not_existing
stat commits-by-day/not_existing/01
stat commits-by-day/2019/not_existing
stat commits-by-day/2019/not_existing/01
stat commits-by-day/2019/09/not_existing
stat commits-by-day/2019/09/not_existing/01
stat commits-by-day/2019/09/24/not_existing
stat commits-by-day/2019/09/24/not_existing/01
stat commits-by-day/2018
stat commits-by-day/2018/09
stat commits-by-day/2019/08
stat commits-by-day/2019/08/08
stat commits-by-day/2019/09/01
