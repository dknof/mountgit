set -x
ls commits-by-month/2019/
ls commits-by-month/2019/09/
cat commits-by-month/2019/09/44aa1e68194596a040e8a5b0cc531411774146e8/message

# references to same month
pushd commits-by-month/2019/09/44aa1e68194596a040e8a5b0cc531411774146e8/ >/dev/null
stat --format '%N %s %f %Y' parents/e24b4a3df3fde6549afb1b5fea9bb77e9a74e378
cat parents/e24b4a3df3fde6549afb1b5fea9bb77e9a74e378/id
stat --format '%N %s %f %Y' parent
cat parent/id
stat --format '%N %s %f %Y' children/6a00fce836a334813663473297c4132bfc6863eb
cat children/6a00fce836a334813663473297c4132bfc6863eb/id
#stat --format '%N %s %f %Y' child
#cat child/id
popd >/dev/null

# references to another month
pushd commits-by-month/2019/10/93ac371b16c247551cff974f9a29ffcc93ddc432/ >/dev/null
stat --format '%N %s %f %Y' parents/*
cat parents/c4aab570b7b58aa8243a567b02fcc6df957a1240/id
stat --format '%N %s %f %Y' parent
cat parent/id
popd >/dev/null

# error checks
stat commits-by-month/not_existing
stat commits-by-month/not_existing/01
stat commits-by-month/2019/not_existing
stat commits-by-month/2019/not_existing/01
stat commits-by-month/2019/09/not_existing
stat commits-by-month/2019/09/not_existing/01
stat commits-by-month/2018
stat commits-by-month/2018/09
stat commits-by-month/2019/02
