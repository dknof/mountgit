#include "constants.h"

#include "filesystem.h"

#include <unistd.h>
#include <sys/stat.h>

auto first_directory(Directory const dir)
  -> Directory
{
  auto p = dir.begin();
  if (p == dir.end())
    return {};
  if (*p != "/")
    return *p;
  ++p;
  if (p == dir.end())
    return {};
  return *p;
}

auto without_first_directory(Path const path)
  -> Path
{
  string const path_str = path;
  if (path_str.empty())
    return {};
  auto p = path_str.begin();
  if (*p == '/')
    ++p;
  while (p != path_str.end() && *p != '/')
    ++p;
  if (p == path_str.end())
    return {};
  if (*p == '/')
    ++p;
  return Path(p, path_str.end());
}

FileStatus::FileStatus() :
  uid(getuid()),
  gid(getgid())
{ }

FileStatus::FileStatus(std::filesystem::file_type type) :
  type(type),
  uid(getuid()),
  gid(getgid())
{ }

void copy(struct stat& lhs, FileStatus const& rhs)
{
  lhs.st_dev     = rhs.dev;
  lhs.st_ino     = rhs.ino;
  lhs.st_mode    = rhs.mode;
  lhs.st_nlink   = rhs.nlink;
  lhs.st_uid     = rhs.uid;
  lhs.st_gid     = rhs.gid;
  lhs.st_rdev    = rhs.rdev;
  lhs.st_size    = rhs.size;
  lhs.st_blksize = rhs.blksize;
  lhs.st_blocks  = rhs.blocks;

  lhs.st_atime = rhs.atime;
  lhs.st_mtime = rhs.mtime;
  lhs.st_ctime = rhs.ctime;

  switch (rhs.type) {
  case file_type::directory:
    lhs.st_mode |= S_IFDIR;
    break;
  case file_type::regular:
    lhs.st_mode |= S_IFREG;
    break;
  case file_type::symlink:
    lhs.st_mode |= S_IFLNK;
    break;
  default:
    break;
  }
}

