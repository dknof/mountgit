#pragma once

#include "gitfuse.h"
#include "../gitpp/commits.h"

class GitFuse::CommitsByMonth {
  public:
    explicit CommitsByMonth(Git::Repository& repository);

    auto getattr(Path path)      -> FileStatus;
    auto readdir(Directory path) -> vector<Path>;
    auto readlink(Path path)     -> Path;
    auto read(File path)         -> string;
  private:
    Git::Commits const commits_;
};
