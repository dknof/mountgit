#include "../constants.h"

#include "tags.h"
#include "../gitpp/tag.h"

GitFuse::Tags::Tags(Git::Repository& repository) :
  repository_(repository)
{ }

auto GitFuse::Tags::repository()
  -> Git::Repository&
{ return repository_; }

auto GitFuse::Tags::getattr(Path const path)
  -> FileStatus
{
  auto const first_dir = first_directory(path);
  auto const remaining_path = without_first_directory(path);
  FileStatus status;
  status.atime = Time();
  status.mtime = Time();

  if (remaining_path == "") {
    status.type = file_type::directory;
    status.mode = 0555;
    status.size = readdir(path).size();
    return status;
  } else {
#if 0
    return Tag(repository(), first_dir, path);
#endif
    status.type = file_type::regular;
    status.mode = 0444;
    status.size = 1024;
  }

  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}

auto GitFuse::Tags::readdir(Directory const dir)
  -> vector<Path>
{
  if (dir == "" || dir == ".") {
    vector<Path> entries;
    for (auto const& tag : repository().tags())
      entries.push_back(tag.name());
    return entries;
  }
  throw filesystem_error("dir not known", dir, std::make_error_code(std::errc::not_a_directory));
}

auto GitFuse::Tags::readlink(Path path)
  -> Path
{
  throw filesystem_error("no link support, yet, in tags", path, std::make_error_code(std::errc::no_link));
}

auto GitFuse::Tags::read(File const path)
  -> string
{
  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}
