#include "../constants.h"

#include "commits-by-day.h"
#include "commit.h"
#include "../gitpp/commit.h"

#include <set>
using std::set;

GitFuse::CommitsByDay::CommitsByDay(Git::Repository& repository) :
  commits_(repository.commits())
{ }

auto GitFuse::CommitsByDay::getattr(Path const path)
  -> FileStatus
{
  if (path == "") {
    FileStatus status;
    status.atime = Time();
    status.mtime = Time();
    status.type = file_type::directory;
    status.mode = 0555;
    status.size = commits_.count();
    return status;
  }

  try {
    auto const year = Year(first_directory(path));
    auto const remaining_path_after_year = without_first_directory(path);
    if (remaining_path_after_year.empty()) {
      if (commits_.count(year) == 0)
        throw filesystem_error("no commit in year", path, std::make_error_code(std::errc::no_such_file_or_directory));
      FileStatus status;
      status.atime = Time();
      status.mtime = Time();
      status.type = file_type::directory;
      status.mode = 0555;
      status.size = commits_.count(year);
      return status;
    }
    auto const month = Month(first_directory(remaining_path_after_year));
    auto const remaining_path_after_month = without_first_directory(remaining_path_after_year);
    if (remaining_path_after_month.empty()) {
      if (commits_.count(year, month) == 0)
        throw filesystem_error("no commit in year, month", path, std::make_error_code(std::errc::no_such_file_or_directory));
      FileStatus status;
      status.atime = Time();
      status.mtime = Time();
      status.type = file_type::directory;
      status.mode = 0555;
      status.size = commits_.count(year, month);
      return status;
    }
    auto const day = Day(first_directory(remaining_path_after_month));
    auto const remaining_path_after_day = without_first_directory(remaining_path_after_month);
    if (remaining_path_after_day.empty()) {
      if (commits_.count(year, month, day) == 0)
        throw filesystem_error("no commit in year, month, day", path, std::make_error_code(std::errc::no_such_file_or_directory));
      FileStatus status;
      status.atime = Time();
      status.mtime = Time();
      status.type = file_type::directory;
      status.mode = 0555;
      status.size = commits_.count(year, month, day);
      return status;
    }

    auto const commit_id = first_directory(remaining_path_after_day);
    auto const remaining_path = without_first_directory(remaining_path_after_day);
    try {
      auto commit = commits_(commit_id);
      auto const time = commit.time();
      if (time.year() != year || time.month() != month || time.day() != day)
        throw filesystem_error("wrong year/month/day", path, std::make_error_code(std::errc::no_such_file_or_directory));
      return Commit(std::move(commit)).getattr(remaining_path);
    } catch (git_error const&) {
      throw filesystem_error("no commit directory: " + commit_id.string(), path, std::make_error_code(std::errc::no_such_file_or_directory));
    }
  } catch (std::invalid_argument const&) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
}

auto GitFuse::CommitsByDay::readdir(Directory const dir)
  -> vector<Path>
{
  if (dir == "" || dir == ".") {
    vector<Path> entries;
    for (auto const year : commits_.years()) {
      entries.push_back(to_string(year));
    }
    return entries;
  }

  try {
    auto const year = Year(first_directory(dir));
    auto const remaining_dir_after_year = without_first_directory(dir);
    if (remaining_dir_after_year.empty()) {
      vector<Path> entries;
      for (auto const month : commits_.months(year)) {
        entries.push_back(to_string(month));
      }
      if (entries.empty())
        throw filesystem_error("no commit in year", dir, std::make_error_code(std::errc::not_a_directory));
      return entries;
    }
    auto const month = Month(first_directory(remaining_dir_after_year));
    auto const remaining_dir_after_month = without_first_directory(remaining_dir_after_year);
    if (remaining_dir_after_month.empty()) {
      vector<Path> entries;
      for (auto const day : commits_.days(year, month)) {
        entries.push_back(to_string(day));
      }
      if (entries.empty())
        throw filesystem_error("no commit in year, month", dir, std::make_error_code(std::errc::not_a_directory));
      return entries;
    }
    auto const day = Day(first_directory(remaining_dir_after_month));
    auto const remaining_dir_after_day = without_first_directory(remaining_dir_after_month);
    if (remaining_dir_after_day.empty()) {
      vector<Path> entries;
      for (auto const commit : commits_(year, month, day)) {
        entries.push_back(commit.id());
      }
      if (entries.empty())
        throw filesystem_error("no commit in year, month, day", dir, std::make_error_code(std::errc::not_a_directory));
      return entries;
    }

    auto const commit_id = first_directory(remaining_dir_after_day);
    auto const remaining_dir = without_first_directory(remaining_dir_after_day);
    try {
      auto commit = commits_(commit_id);
      auto const time = commit.time();
      if (time.year() != year || time.month() != month || time.day() != day)
        throw filesystem_error("wrong year/month/day", dir, std::make_error_code(std::errc::not_a_directory));
      if (commits_.count(year, month, day) == 0)
        throw filesystem_error("no commit in year, month, day", dir, std::make_error_code(std::errc::not_a_directory));
      return Commit(commit).readdir(remaining_dir);
    } catch (git_error const&) {
      throw filesystem_error("dir not known", dir, std::make_error_code(std::errc::not_a_directory));
    }
  } catch (std::invalid_argument const&) {
    throw filesystem_error("dir not known", dir, std::make_error_code(std::errc::not_a_directory));
  }
}

auto GitFuse::CommitsByDay::readlink(Path path)
  -> Path
{
  auto const year = Year(first_directory(path));
  auto const remaining_path_after_year = without_first_directory(path);
  if (remaining_path_after_year.empty()) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  auto const month = Month(first_directory(remaining_path_after_year));
  auto const remaining_path_after_month = without_first_directory(remaining_path_after_year);
  if (remaining_path_after_month.empty()) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  auto const day = Day(first_directory(remaining_path_after_month));
  auto const remaining_path_after_day = without_first_directory(remaining_path_after_month);
  if (remaining_path_after_day.empty()) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  auto const commit_id = first_directory(remaining_path_after_day);
  auto const remaining_path = without_first_directory(remaining_path_after_day);
  if (remaining_path.empty())
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  try {
    auto commit = commits_(commit_id);
    auto const time = commit.time();
    if (time.year() != year || time.month() != month || time.day() != day)
      throw filesystem_error("wrong year/month/day", path, std::make_error_code(std::errc::no_such_file_or_directory));
    auto const subdir = first_directory(remaining_path);
    if (subdir == "files")
      return Commit(std::move(commit)).readlink(remaining_path);
    if (subdir == "parent" || subdir == "parents" || subdir == "child" || subdir == "children") {
      auto const ref_id = Commit(commit).commit_id(remaining_path);
      auto const ref_time = commits_(ref_id).time();
      auto const base_path = (subdir == "parent" || subdir == "child" ? Path("..") : Path("../.."));
      if (ref_time.year() != year)
        return base_path/".."/".."/".."/to_string(ref_time.year())/to_string(ref_time.month())/to_string(ref_time.day())/static_cast<string>(ref_id);
      if (ref_time.month() != month)
        return base_path/".."/".."/to_string(ref_time.month())/to_string(ref_time.day())/static_cast<string>(ref_id);
      if (ref_time.day() != day)
        return base_path/".."/to_string(ref_time.day())/static_cast<string>(ref_id);
      return base_path/static_cast<string>(ref_id);
    }
  } catch (git_error const& error) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}

auto GitFuse::CommitsByDay::read(File const path)
  -> string
{
  auto const year = Year(first_directory(path));
  auto const remaining_path_after_year = without_first_directory(path);
  if (remaining_path_after_year.empty()) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  auto const month = Month(first_directory(remaining_path_after_year));
  auto const remaining_path_after_month = without_first_directory(remaining_path_after_year);
  if (remaining_path_after_month.empty()) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  auto const day = Day(first_directory(remaining_path_after_month));
  auto const remaining_path_after_day = without_first_directory(remaining_path_after_month);
  if (remaining_path_after_day.empty()) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  auto const commit_id = first_directory(remaining_path_after_day);
  auto const remaining_path = without_first_directory(remaining_path_after_day);
  if (remaining_path.empty())
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  try {
    auto commit = commits_(commit_id);
    auto const time = commit.time();
    if (time.year() != year || time.month() != month || time.day() != day)
      throw filesystem_error("wrong year/month/day", path, std::make_error_code(std::errc::no_such_file_or_directory));
    return Commit(std::move(commit)).read(remaining_path);
  } catch (git_error const& error) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}
