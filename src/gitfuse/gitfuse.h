#pragma once

#include "../fuse.h"
#include "../gitpp/repository.h"

class GitFuse : public Fuse {
  public:
    class Tags;
    class Tag;
    class Commits;
    class CommitsByMonth;
    class CommitsByDay;
    class Commit;
    class Tree;
    class Branches;
  public:
    GitFuse(Git::Repository&& repository, Directory mountpoint,
            vector<string> arguments = {});
    GitFuse(Directory repository, Directory mountpoint,
            vector<string> arguments = {});
    GitFuse(Fuse const&) = delete;
    GitFuse& operator=(GitFuse const&) = delete;
    ~GitFuse();

    auto repository() -> Git::Repository&;

    auto getattr(Path path)       -> FileStatus   override;
    auto readdir(Directory path)  -> vector<Path> override;
    auto readlink(Path path)      -> Path         override;
    auto read(File path)          -> string       override;
  private:
    Git::Repository repository_;
};
