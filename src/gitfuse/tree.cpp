#include "../constants.h"

#include "tree.h"
#include "../gitpp/tree_entry.h"

GitFuse::Tree::Tree(Git::Tree const& tree) :
  tree_(tree)
{ }

GitFuse::Tree::Tree(Git::Tree&& tree) :
  tree_(std::move(tree))
{ }

auto GitFuse::Tree::tree()
  -> Git::Tree&
{ return tree_; }

auto GitFuse::Tree::getattr(Path const path)
  -> FileStatus
{
  if (path.empty()) {
    FileStatus status;
    status.type = file_type::directory;
    status.mode = 0555;
    status.size = readdir("").size();
    return status;
  }
  return tree_.entry(path).stat();

  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}

auto GitFuse::Tree::readdir(Directory const dir)
  -> vector<Path>
{
  if (dir == "") {
    auto const entries = tree_.entries();
    vector<Path> paths;
    paths.reserve(entries.size());
    for (auto const& entry : entries)
      paths.emplace_back(entry.name());
    return paths;
  }
  try {
    auto const entries = tree_.entries(dir);
    vector<Path> paths;
    paths.reserve(entries.size());
    for (auto const& entry : entries) {
      paths.emplace_back(entry.name());
    }
    return paths;
  } catch (...) {
    throw filesystem_error("dir not known", dir, std::make_error_code(std::errc::not_a_directory));
  }
}

auto GitFuse::Tree::readlink(Path const path)
  -> Path
{
  try {
    auto const entry = tree_.entry(path);
    return entry.content();
  } catch (...) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
}

auto GitFuse::Tree::read(File const path)
  -> string
{
  try {
    auto const entry = tree_.entry(path);
    return entry.content();
  } catch (...) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
}
