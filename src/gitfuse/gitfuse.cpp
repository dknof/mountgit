#include "../constants.h"
#include "gitfuse.h"
#include "branches.h"
#include "commits.h"
#include "commits.h"
#include "commits-by-month.h"
#include "commits-by-day.h"
#include "tags.h"

GitFuse::GitFuse(Git::Repository&& repository, Directory const mountpoint,
                 vector<string> arguments) :
  Fuse(mountpoint,
       [&arguments]() -> auto {
       arguments.push_back("-o");
       arguments.push_back("ro");
       return arguments;}()),
  repository_(std::move(repository))
{ }

GitFuse::GitFuse(Directory const repository, Directory const mountpoint,
                 vector<string> const arguments) :
  GitFuse(Git::Repository(repository), mountpoint, arguments)
{ }


GitFuse::~GitFuse() = default;


auto GitFuse::repository()
  -> Git::Repository&
{ return repository_; }


auto GitFuse::getattr(Path const path)
  -> FileStatus
{
  FileStatus status;
  status.atime = Time();
  status.mtime = Time();

  if (path == "/"s) {
    status.type = file_type::directory;
    status.mode = 0555;
    status.nlink = 2;
    status.size = readdir(path).size();
    return status;
  } else if (first_directory(path) == "branches") {
    return Branches(repository()).getattr(without_first_directory(path));
  } else if (first_directory(path) == "commits") {
    return Commits(repository()).getattr(without_first_directory(path));
  } else if (first_directory(path) == "commits-by-month") {
    return CommitsByMonth(repository()).getattr(without_first_directory(path));
  } else if (first_directory(path) == "commits-by-day") {
    return CommitsByDay(repository()).getattr(without_first_directory(path));
  } else if (first_directory(path) == "tags") {
    return Tags(repository()).getattr(without_first_directory(path));
  } else {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  return {};
}


auto GitFuse::readdir(Directory const dir)
  -> vector<Path>
{
  if (dir == "/"s) {
    return {"branches", "commits", "commits-by-month", "commits-by-day", "tags"};
  } else if (dir == "/branches"s) {
    return Branches(repository()).readdir(".");
  } else if (dir == "/commits"s) {
    return Commits(repository()).readdir(".");
  } else if (dir == "/commits-by-month"s) {
    return CommitsByMonth(repository()).readdir(".");
  } else if (dir == "/commits-by-day"s) {
    return CommitsByDay(repository()).readdir(".");
  } else if (dir == "/tags"s) {
    return Tags(repository()).readdir(".");
  }

  auto const first_dir = first_directory(dir);
  auto const remaining_path = without_first_directory(dir);
  if (first_dir == "branches") {
    return Branches(repository()).readdir(remaining_path);
  } else if (first_dir == "commits") {
    return Commits(repository()).readdir(remaining_path);
  } else if (first_dir == "commits-by-month") {
    return CommitsByMonth(repository()).readdir(remaining_path);
  } else if (first_dir == "commits-by-day") {
    return CommitsByDay(repository()).readdir(remaining_path);
  } else if (first_dir == "tags") {
    return Tags(repository()).readdir(remaining_path);
  }
  throw filesystem_error("dir not known", dir, std::make_error_code(std::errc::not_a_directory));
}

auto GitFuse::readlink(Path path)
  -> Path
{
  auto const first_dir = first_directory(path);
  auto const remaining_path = without_first_directory(path);
  if (first_dir == "branches") {
    return Branches(repository()).readlink(remaining_path);
  } else if (first_dir == "commits") {
    return Commits(repository()).readlink(remaining_path);
  } else if (first_dir == "commits-by-month") {
    return CommitsByMonth(repository()).readlink(remaining_path);
  } else if (first_dir == "commits-by-day") {
    return CommitsByDay(repository()).readlink(remaining_path);
  } else if (first_dir == "tags") {
    return Tags(repository()).readlink(remaining_path);
  }
  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}

auto GitFuse::read(File const path)
  -> string
{
  auto const first_dir = first_directory(path);
  auto const remaining_path = without_first_directory(path);
  if (first_dir == "branches") {
    return Branches(repository()).read(remaining_path);
  } else if (first_dir == "commits") {
    return Commits(repository()).read(remaining_path);
  } else if (first_dir == "commits-by-month") {
    return CommitsByMonth(repository()).read(remaining_path);
  } else if (first_dir == "commits-by-day") {
    return CommitsByDay(repository()).read(remaining_path);
  } else if (first_dir == "tags") {
    return Tags(repository()).read(remaining_path);
  }
  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}
