string const help_text = ""
"mountgit: mounting a git repository as readonly filesystem\n"
"usage: mountgit <git_repository> <mount_point> [<FUSE library options>]\n"
"       mountgit --help\n"
"       mountgit --version\n"
"with\n"
"  git_repository   -- path to a git repository (working or bare)\n"
"  mount_point      --  directory to mount into\n"
"  --help           --  show the help\n"
"  --version        --  show the version\n"
"Common FUSE library options are:\n"
"  -f               -- run in foreground, do not daemonize\n"
"  -d               -- run in foreground and print debug information\n"
"  -s               -- run single-threaded\n"
;
