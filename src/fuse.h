#pragma once

class Fuse {
  public:
    Fuse(Directory mountpoint, vector<string> options = {});
    Fuse(Fuse const&) = delete;
    Fuse& operator=(Fuse const&) = delete;
    virtual ~Fuse();

    auto run() -> int;

    virtual auto getattr(Path path)       -> FileStatus   = 0;
    virtual auto readdir(Directory path)  -> vector<Path> = 0;
    virtual auto readlink(Path path)      -> Path;
    virtual auto read(File path)          -> string       = 0;
#ifdef TODO
    virtual auto read(File path, size_t pos, off_t offset)          -> string       = 0;
    virtual void open(File path, FileInfo&);
    virtual void release(File path, FileInfo&);
    virtual auto statfs(Path path) -> StatVFS;
    virtual void opendir(Directory path, FileInfo&);
    virtual void readdir(Directory path, FileInfo&);
    virtual void releasedir(Directory path, FileInfo&);

#endif
  private:
    Directory mountpoint_;
    vector<string> options_;
};
