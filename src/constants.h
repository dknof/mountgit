// ./constants.h
#pragma once

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
#include <string>
using std::string;
using namespace std::literals::string_literals;
#include <vector>
using std::vector;
#include <memory>
using std::unique_ptr;
using std::make_unique;
#include <filesystem>
using Directory = std::filesystem::path;
using File = std::filesystem::path;
using Path = std::filesystem::path;
using std::filesystem::file_type;
using std::filesystem::filesystem_error;
#include "filesystem.h"
#include <algorithm>
using std::min;
namespace std {
template<class Container, class Value>
constexpr auto find(Container const& container, Value const& value)
{ return find(begin(container), end(container), value); }
template<class Container, class UnaryPredicate>
constexpr auto find_if(Container const& container, UnaryPredicate p)
{ return find_if(begin(container), end(container), p); }
template<class Container, class UnaryPredicate>
constexpr auto count_if(Container const& container, UnaryPredicate p)
{ return count_if(begin(container), end(container), p); }
template<class Container, class UnaryPredicate>
constexpr bool contains(Container const& container, UnaryPredicate p)
{ return (find_if(container, p) != end(container)); }
}

#include <cassert>

#include <git2.h>
#include "time.h"

extern std::ostream* err_ostr;
extern std::ostream* log_ostr;
#define CERR *err_ostr << __FILE__ << '#' << __LINE__ << ' '
#define CLOG *log_ostr << __FILE__ << '#' << __LINE__ << ' '

#include "options.h"
