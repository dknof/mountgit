#include "constants.h"

#include "gitfuse/gitfuse.h"

#include "gitpp/repository.h"

#include <fstream>

Options options;

//#define ERROR_OUTPUT_FILE "mountgit.err"
//#define DEBUG_OUTPUT_FILE "mountgit.log"


#ifdef DEBUG_OUTPUT_FILE
std::ostream* log_ostr = new std::ofstream(DEBUG_OUTPUT_FILE, std::ios_base::app);
#else
std::ostream* log_ostr = &std::clog;
#endif

#ifdef ERROR_OUTPUT_FILE
std::ostream* err_ostr = new std::ofstream(ERROR_OUTPUT_FILE, std::ios_base::app);
#else
std::ostream* err_ostr = &cerr;
#endif

#include "help.h"

auto main(int argc, char* argv[])
  -> int
{
  if (argc == 2) {
    if (   argv[1] == "--help"s
        || argv[1] == "-h"s
        || argv[1] == "-?"s) {
      cout << help_text << '\n';
      return 0;
    }
    if (   argv[1] == "--version"s
        || argv[1] == "-v"s) {
      cout << "mountgit\n";
      cout << "version:  " << "0.x" << '\n';
      cout << "compiled: " << __DATE__ << '\n';
      return 0;
    }
  }
  if (argc <= 2) {
    cerr << "unknown call of umountgit\n";
    cerr << help_text << '\n';
    return 1;
  }
  try {
    Git::Repository repository(argv[1]);
    Directory const mount_point(argv[2]);
    vector<string> arguments;
    for (int i = 3; i < argc; ++i)
      arguments.push_back(argv[i]);
    GitFuse git_fuse(std::move(repository), mount_point, arguments);
    return git_fuse.run();
  } catch (git_error const& error) {
    cerr << error.message << std::endl;
    throw;
  }
}
