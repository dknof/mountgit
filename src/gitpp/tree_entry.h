#pragma once

#include "tree.h"
namespace Git {
class Repository;
}

struct git_tree_entry;

namespace Git {
class TreeEntry {
  public:
    TreeEntry(Tree const& tree, Path path);
    TreeEntry(TreeEntry const& entry);
    TreeEntry(Tree const& tree, git_tree_entry const* entry);
    TreeEntry(TreeEntry&& entry);
    auto operator=(TreeEntry const& entry) -> TreeEntry& = delete;
    auto operator=(TreeEntry&& entry)      -> TreeEntry& = delete;
    ~TreeEntry();

    auto c_ptr() const      -> git_tree_entry const*;
    operator git_tree_entry const*() const;
    operator git_oid const*()        const;
    auto repository() const -> Repository const&;

    auto name() const      -> string;
    auto file_type() const -> std::filesystem::file_type;
    auto stat() const      -> FileStatus;
    auto content() const   -> string;

  private:
    Repository const& repository_;
    mutable git_tree_entry const* entry_ = nullptr;
};
}
