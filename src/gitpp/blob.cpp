#include "../constants.h"
#include "blob.h"
#include "repository.h"
#include "tree_entry.h"

#include <git2.h>

namespace {
  static char oid_buffer[GIT_OID_HEXSZ+1];
}

namespace Git {

Blob::Blob(TreeEntry const& entry) :
  repository_(entry.repository())
{
  auto oid = git_tree_entry_id(entry);
  auto error = git_blob_lookup(const_cast<git_blob**>(&blob_),
                               const_cast<git_repository*>(repository_.c_ptr()),
                               oid);
  if (error)
    throw *giterr_last();
}

Blob::Blob(Repository const& repository, git_oid const& oid) :
  repository_(repository)
{
  auto error = git_blob_lookup(const_cast<git_blob**>(&blob_),
                               const_cast<git_repository*>(repository_.c_ptr()),
                               &oid);
  if (error)
    throw *giterr_last();
}

Blob::Blob(Blob&& blob) :
  repository_(blob.repository_),
  blob_(blob.blob_)
{
  blob.blob_ = nullptr;
}

Blob::~Blob()
{
  git_blob_free(const_cast<git_blob*>(blob_));
}

auto Blob::c_ptr() const
-> git_blob const*
{
  return blob_;
}

Blob::operator git_blob const*()
{
  return blob_;
}

auto Blob::repository() const
-> Repository const&
{
  return repository_;
}

auto Blob::id() const
-> string
{
  return git_oid_tostr(oid_buffer, GIT_OID_HEXSZ+1, git_blob_id(blob_));
}

auto
Blob::content() const
-> string
{
  return string(static_cast<char const*>(git_blob_rawcontent(blob_)), git_blob_rawsize(blob_));
}

auto
Blob::size() const
-> size_t
{
  return git_blob_rawsize(blob_);
}

} // namespace Git
