#include "../constants.h"
#include "branch.h"
#include "repository.h"
#include "commit.h"

namespace Git {
Branch::Branch(Repository const& repository, string const name) :
  repository_(repository)
{
  auto error = git_branch_lookup(&branch_,
                                 const_cast<git_repository*>(repository.c_ptr()),
                                 name.c_str(),
                                 GIT_BRANCH_LOCAL);
  if (error) {
    throw *giterr_last();
  }
}

Branch::Branch(Branch&& branch) :
  repository_(branch.repository_),
  branch_(branch.branch_)
{
  branch.branch_ = nullptr;
}

Branch::~Branch()
{
  git_reference_free(branch_);
}

auto Branch::c_ptr() const
-> git_reference const*
{
  return branch_;
}

Branch::operator git_reference const*() const
{
  return branch_;
}

auto Branch::repository() const
-> Repository const&
{
  return repository_;
}

auto Branch::name() const
-> string
{
  char const* name = nullptr;
  auto error = git_branch_name(&name, branch_);
  if (error)
    throw *giterr_last();
  return name;
}

auto Branch::commit() const
-> Commit
{
  auto repository = git_reference_owner(branch_);
  git_oid oid;
  auto error = git_reference_name_to_id(&oid, repository, ("refs/heads/" + name()).c_str());
  if (error)
    throw *giterr_last();
  return {repository, oid};
}

} // namespace Git
