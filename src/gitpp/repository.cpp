#include "../constants.h"

#include "repository.h"
#include "branch.h"
#include "commits.h"
#include "commit.h"
#include "tag.h"

namespace Git {
Repository::Repository(Directory const path)
{
  static bool git_initialized = false;
  if (!git_initialized) {
    git_libgit2_init();
    git_initialized = true;
  }
  auto error = git_repository_open(&repository_, path.c_str());
  if (error)
    throw *giterr_last();
  managed_ = true;
}

Repository::Repository(git_repository* repository) :
  repository_(repository),
  managed_(false)
{ }

Repository::Repository(Repository&& repository) :
  repository_(repository.repository_),
  managed_(repository.managed_)
{
  repository.repository_ = nullptr;
  repository.managed_ = false;
}

auto Repository::operator=(Repository&& repository)
  -> Repository&
{
  repository_ = repository.repository_;
  managed_ = repository.managed_;
  repository.repository_ = nullptr;
  repository.managed_ = false;
  return *this;
}

Repository::~Repository()
{
  if (managed_) {
    git_repository_free(repository_);
  }
}

auto Repository::c_ptr()
  -> git_repository*
{
  return repository_;
}

auto Repository::c_ptr() const
-> git_repository const*
{
  return repository_;
}

Repository::operator git_repository*()
{
  return repository_;
}

Repository::operator git_repository const*() const
{
  return repository_;
}

auto Repository::workdir() const
-> string
{
  return git_repository_workdir(repository_);
}

auto Repository::branches() const
-> vector<Branch>
{
  git_reference_iterator* iter = nullptr;
  auto error = git_reference_iterator_glob_new(&iter, repository_, "refs/heads/*");
  if (error)
    throw *giterr_last();

  vector<Branch> references;
  git_reference* ref = nullptr;
  while (!(error = git_reference_next(&ref, iter))) {
    references.emplace_back(*this, git_reference_name(ref) + "refs/heads/"s.size());
    git_reference_free(ref);
  }
  git_reference_iterator_free(iter);
  if (error != GIT_ITEROVER)
    throw *giterr_last();

  return references;
}

auto Repository::branch(string const name) const
-> Branch
{
  return {*this, name};
}

auto Repository::commits() const
-> Commits
{
  return Commits(*this);
}

auto Repository::commit(string const id) const
-> Commit
{
  return Commit(*this, id);
}

auto Repository::tags() const
-> vector<Tag>
{
  git_strarray list;
  auto error = git_tag_list(&list, repository_);
  if (error)
    throw *giterr_last();

  vector<Tag> tags;
  tags.reserve(list.count);
  cout << list.count << '\n';
  for (size_t i = 0; i < list.count; ++i)
    tags.emplace_back(*this, list.strings[i]);

  git_strarray_free(&list);
  return tags;
}

#ifdef TODO
auto Repository::tag(string const name) const
-> Tag
{
  return {};
}
#endif

}
