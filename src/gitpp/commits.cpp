#include "../constants.h"
#include "commits.h"
#include "commit.h"
#include "repository.h"

#include <set>
using std::set;

#include <git2.h>

namespace Git {
Commits::Commits(Repository const& repository) :
  repository_(repository)
{ }

Commits::Commits(Commits const& commits) :
  repository_(commits.repository_)
{ }

Commits::Commits(Commits&& commits) :
  repository_(commits.repository_)
{ }

Commits::~Commits()
{ }

Commits::operator vector<Commit>() const
{
  git_revwalk *walk;
  {
    auto error = git_revwalk_new(&walk, const_cast<git_repository*>(repository_.c_ptr()));
    if (error)
      throw *giterr_last();
  }

  {
    auto error = git_revwalk_push_glob(walk, "*");
    if (error)
      throw *giterr_last();
  }
  git_oid oid;
  vector<Commit> commits;
  while ((git_revwalk_next(&oid, walk)) == 0) {
    commits.emplace_back(repository_, oid);
  }
  git_revwalk_free(walk);
  return commits;
}

auto Commits::operator()(string id) const
-> Commit
{
  return Commit(repository_, id);
}

auto Commits::operator()(Year year) const
-> vector<Commit>
{
  git_revwalk *walk;
  {
    auto error = git_revwalk_new(&walk, const_cast<git_repository*>(repository_.c_ptr()));
    if (error)
      throw *giterr_last();
  }

  {
    auto error = git_revwalk_push_glob(walk, "*");
    if (error)
      throw *giterr_last();
  }
  git_oid oid;
  vector<Commit> commits;
  while ((git_revwalk_next(&oid, walk)) == 0) {
    auto const commit = Commit(repository_, oid);
    if (commit.time().year() != year)
      continue;
    commits.emplace_back(std::move(commit));
  }
  git_revwalk_free(walk);
  return commits;
}

auto Commits::operator()(Year year, Month month) const
-> vector<Commit>
{
  git_revwalk *walk;
  {
    auto error = git_revwalk_new(&walk, const_cast<git_repository*>(repository_.c_ptr()));
    if (error)
      throw *giterr_last();
  }

  {
    auto error = git_revwalk_push_glob(walk, "*");
    if (error)
      throw *giterr_last();
  }
  git_oid oid;
  vector<Commit> commits;
  while ((git_revwalk_next(&oid, walk)) == 0) {
    auto const commit = Commit(repository_, oid);
    if (   commit.time().year()  != year
        || commit.time().month() != month) {
      continue;
    }
    commits.emplace_back(std::move(commit));
  }
  git_revwalk_free(walk);
  return commits;
}

auto Commits::operator()(Year year, Month month, Day day) const
-> vector<Commit>
{
  git_revwalk *walk;
  {
    auto error = git_revwalk_new(&walk, const_cast<git_repository*>(repository_.c_ptr()));
    if (error)
      throw *giterr_last();
  }

  {
    auto error = git_revwalk_push_glob(walk, "*");
    if (error)
      throw *giterr_last();
  }
  git_oid oid;
  vector<Commit> commits;
  while ((git_revwalk_next(&oid, walk)) == 0) {
    auto const commit = Commit(repository_, oid);
    if (   commit.time().year()  != year
        || commit.time().month() != month
        || commit.time().day()   != day) {
      continue;
    }
    commits.emplace_back(std::move(commit));
  }
  git_revwalk_free(walk);
  return commits;
}

auto Commits::count() const
-> size_t
{
  git_revwalk *walk;
  {
    auto error = git_revwalk_new(&walk, const_cast<git_repository*>(repository_.c_ptr()));
    if (error)
      throw *giterr_last();
  }

  {
    auto error = git_revwalk_push_glob(walk, "*");
    if (error)
      throw *giterr_last();
  }
  git_oid oid;
  size_t count = 0;
  while ((git_revwalk_next(&oid, walk)) == 0) {
    count += 1;
  }
  git_revwalk_free(walk);
  return count;
}

auto Commits::count(Year year) const
-> size_t
{
  git_revwalk *walk;
  {
    auto error = git_revwalk_new(&walk, const_cast<git_repository*>(repository_.c_ptr()));
    if (error)
      throw *giterr_last();
  }

  {
    auto error = git_revwalk_push_glob(walk, "*");
    if (error)
      throw *giterr_last();
  }
  git_oid oid;
  size_t count = 0;
  while ((git_revwalk_next(&oid, walk)) == 0) {
    auto const commit = Commit(repository_, oid);
    if (commit.time().year() != year)
      continue;
    count += 1;
  }
  git_revwalk_free(walk);
  return count;
}

auto Commits::count(Year year, Month month) const
-> size_t
{
  git_revwalk *walk;
  {
    auto error = git_revwalk_new(&walk, const_cast<git_repository*>(repository_.c_ptr()));
    if (error)
      throw *giterr_last();
  }

  {
    auto error = git_revwalk_push_glob(walk, "*");
    if (error)
      throw *giterr_last();
  }
  git_oid oid;
  size_t count = 0;
  while ((git_revwalk_next(&oid, walk)) == 0) {
    auto const commit = Commit(repository_, oid);
    if (   commit.time().year()  != year
        || commit.time().month() != month) {
      continue;
    }
    count += 1;
  }
  git_revwalk_free(walk);
  return count;
}

auto Commits::count(Year year, Month month, Day day) const
-> size_t
{
  git_revwalk *walk;
  {
    auto error = git_revwalk_new(&walk, const_cast<git_repository*>(repository_.c_ptr()));
    if (error)
      throw *giterr_last();
  }

  {
    auto error = git_revwalk_push_glob(walk, "*");
    if (error)
      throw *giterr_last();
  }
  git_oid oid;
  size_t count = 0;
  while ((git_revwalk_next(&oid, walk)) == 0) {
    auto const commit = Commit(repository_, oid);
    if (   commit.time().year()  == year
        && commit.time().month() == month
        && commit.time().day()   == day) {
      count += 1;
    }
  }
  git_revwalk_free(walk);
  return count;
}

auto Commits::years() const
-> vector<Year>
{
  git_revwalk *walk;
  {
    auto error = git_revwalk_new(&walk, const_cast<git_repository*>(repository_.c_ptr()));
    if (error)
      throw *giterr_last();
  }

  {
    auto error = git_revwalk_push_glob(walk, "*");
    if (error)
      throw *giterr_last();
  }
  git_oid oid;
  set<Year> years;
  while ((git_revwalk_next(&oid, walk)) == 0) {
    auto const commit = Commit(repository_, oid);
    years.insert(commit.time().year());
  }
  git_revwalk_free(walk);

  vector<Year> years_vector(years.begin(), years.end());
  std::sort(years_vector.begin(), years_vector.end());
  return years_vector;
}

auto Commits::months(Year year) const
-> vector<Month>
{
  git_revwalk *walk;
  {
    auto error = git_revwalk_new(&walk, const_cast<git_repository*>(repository_.c_ptr()));
    if (error)
      throw *giterr_last();
  }

  {
    auto error = git_revwalk_push_glob(walk, "*");
    if (error)
      throw *giterr_last();
  }
  git_oid oid;
  set<Month> months;
  while ((git_revwalk_next(&oid, walk)) == 0) {
    auto const commit = Commit(repository_, oid);
    if (commit.time().year() != year)
      continue;
    months.insert(commit.time().month());
  }
  git_revwalk_free(walk);

  vector<Month> months_vector(months.begin(), months.end());
  std::sort(months_vector.begin(), months_vector.end());
  return months_vector;
}

auto Commits::days(Year year, Month month) const
-> vector<Day>
{
  git_revwalk *walk;
  {
    auto error = git_revwalk_new(&walk, const_cast<git_repository*>(repository_.c_ptr()));
    if (error)
      throw *giterr_last();
  }

  {
    auto error = git_revwalk_push_glob(walk, "*");
    if (error)
      throw *giterr_last();
  }
  git_oid oid;
  set<Day> days;
  while ((git_revwalk_next(&oid, walk)) == 0) {
    auto const commit = Commit(repository_, oid);
    if (   commit.time().year()  != year
        || commit.time().month() != month) {
      continue;
    }
    days.insert(commit.time().day());
  }
  git_revwalk_free(walk);

  vector<Day> days_vector(days.begin(), days.end());
  std::sort(days_vector.begin(), days_vector.end());
  return days_vector;
}

}
