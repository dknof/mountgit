#include "../constants.h"
#include "tag.h"
#include "repository.h"
#include "signature.h"

#include <git2.h>

namespace Git {
Tag::Tag(Repository const& repository, string const name) :
  name_(name)
{
  git_object* object = nullptr;
  auto error = git_revparse_single(&object, const_cast<git_repository*>(repository.c_ptr()), name.c_str());
  if (error)
    throw *giterr_last();

  switch (git_object_type(object)) {
  case GIT_OBJ_TAG:
    tag_ = reinterpret_cast<git_tag*>(object);
    break;
  case GIT_OBJ_COMMIT:
    commit_ = reinterpret_cast<git_commit*>(object);
    break;
  default:
    git_object_free(object);
    throw "object type is not a tag"s;
  }
}

Tag::Tag(Tag&& tag) :
  tag_(tag.tag_),
  commit_(tag.commit_),
  name_(std::move(tag.name_))
{
  tag.tag_ = nullptr;
  tag.commit_ = nullptr;
}

auto Tag::operator=(Tag&& tag)
  -> Tag&
{
  tag_ = tag.tag_;
  tag.tag_ = nullptr;
  commit_ = tag.commit_;
  tag.commit_ = nullptr;
  name_ = std::move(tag.name_);
  return *this;
}

Tag::~Tag()
{
  git_tag_free(tag_);
  git_commit_free(commit_);
}

auto Tag::c_ptr() const -> git_tag const*
{
  if (tag_)
    return tag_;
  throw "Tag::c_ptr(): is no git_tag"s;
}

auto Tag::name() const -> string
{
  if (tag_)
    return git_tag_name(tag_);
  return name_;
}

auto Tag::message() const -> string
{
  if (tag_)
    return git_tag_message(tag_);
  if (commit_)
    return git_commit_message(commit_);
  throw "Tag::message(): neither tag not commit set"s;
}

auto Tag::repository() const -> Repository
{
  if (tag_)
    return {git_tag_owner(tag_)};
  if (commit_)
    return {git_commit_owner(commit_)};
  throw "Tag::repository(): neither tag not commit set"s;
}

auto Tag::signature() const -> Signature
{
  if (tag_)
    return {git_tag_tagger(tag_)};
  if (commit_)
    return {git_commit_committer(commit_)};
  throw "Tag::signature(): neither tag not commit set"s;
}

} // namespace Git
