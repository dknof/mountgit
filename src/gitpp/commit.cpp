#include "../constants.h"
#include "commit.h"
#include "repository.h"

#include <git2.h>

namespace {
  static char oid_buffer[GIT_OID_HEXSZ+1];
}

namespace Git {
Commit::Commit(Repository const& repository, string sha) :
  repository_(repository)
{
  git_oid oid;
  auto error = git_oid_fromstr(&oid, sha.c_str());
  if (error)
    throw *giterr_last();
  error = git_commit_lookup(const_cast<git_commit**>(&commit_),
                            const_cast<git_repository*>(repository.c_ptr()),
                            &oid);
  if (error)
    throw *giterr_last();
}

Commit::Commit(Repository const& repository, git_oid const& oid) :
  repository_(repository)
{
  auto error = git_commit_lookup(const_cast<git_commit**>(&commit_),
                                 const_cast<git_repository*>(repository.c_ptr()),
                                 &oid);
  if (error)
    throw *giterr_last();
}

Commit::Commit(Commit const& commit) :
  repository_(commit.repository_)
{
  auto error = git_commit_dup(const_cast<git_commit**>(&commit_), const_cast<git_commit*>(commit.c_ptr()));
  if (error)
    throw *giterr_last();
}

Commit::Commit(Commit&& commit) :
  repository_(commit.repository_),
  commit_(commit.commit_)
{
  commit.commit_ = nullptr;
}

Commit::~Commit()
{
  git_commit_free(const_cast<git_commit*>(commit_));
}

auto Commit::c_ptr() const
-> git_commit const*
{
  return commit_;
}

Commit::operator git_commit const*() const
{
  return commit_;
}

auto Commit::repository() const
-> Repository const&
{
  return repository_;
}

auto Commit::id() const
-> Id
{
  auto oid = git_commit_id(commit_);
  return git_oid_tostr(oid_buffer, GIT_OID_HEXSZ+1, oid);
}

auto Commit::message() const
-> string
{
  return git_commit_message(commit_);
}

auto Commit::summary() const
-> string
{
  return git_commit_summary(const_cast<git_commit*>(commit_));
}

auto Commit::body() const
-> string
{
  auto const body = git_commit_body(const_cast<git_commit*>(commit_));
  return (body ? body : ""s);
}

auto Commit::time() const
-> Time
{
  return git_commit_time(commit_);
}

auto Commit::committer() const
-> Signature
{
  return {git_commit_committer(commit_)};
}

auto Commit::author() const
-> Signature
{
  return {git_commit_author(commit_)};
}

auto
Commit::parents() const
-> vector<Commit>
{
  vector<Commit> parents;
  auto const n = git_commit_parentcount(commit_);
  for (size_t i = 0; i < n; ++i)
    parents.emplace_back(repository_, *git_commit_parent_id(commit_, i));

  return parents;
}

auto
Commit::children() const
-> vector<Commit>
{
  git_revwalk *walk;
  {
    auto error = git_revwalk_new(&walk, const_cast<git_repository*>(repository_.c_ptr()));
    if (error)
      throw *giterr_last();
  }

  vector<Commit> children;
  {
    auto error = git_revwalk_push_glob(walk, "*");
    if (error)
      throw *giterr_last();
  }
  auto const id_string = id();
  auto is_commit = [&id_string](Commit const& parent) {
    return parent.id() == id_string;
  };
  git_oid oid;
  while ((git_revwalk_next(&oid, walk)) == 0) {
    try {
      auto commit = Commit(repository_, oid);
      auto const parents = commit.parents();
      if (std::contains(parents, is_commit))
        children.emplace_back(std::move(commit));
    } catch (git_error) {
    }
  }
  git_revwalk_free(walk);
  return children;
}

} // namespace Git
