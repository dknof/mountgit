#include "constants.h"
#include "utils.h"

#include <cstring>

auto convert_into_argv(string const program_name, vector<string> const args)
  -> char**
{
  char** argv;
  argv = new char*[args.size() + 2];
  argv[0] = new char[program_name.size() + 1];
  strcpy(argv[0], program_name.c_str());
  for (size_t i = 0; i < args.size(); ++i) {
    argv[i + 1] = new char[args[i].size() + 1];
    strcpy(argv[i + 1], args[i].c_str());
  }
  argv[args.size() + 1] = nullptr;
  return argv;
}

auto count_argv(char** const argv)
  -> int
{
  int i = 0;
  while (argv[i])
    ++i;
  return i;
}

void free_argv(char** argv)
{
  for (auto a = argv; *a; ++a)
    delete[] *a;
  delete argv;
}

