#pragma once

auto first_directory(Directory dir)     -> Directory;
auto without_first_directory(Path path) -> Path;

struct FileStatus { // see man 2 stat
  FileStatus();
  explicit FileStatus(std::filesystem::file_type type);

  std::filesystem::file_type type;
  dev_t     dev = 0;         /* ID of device containing file */
  ino_t     ino = 0;         /* Inode number */
  mode_t    mode = 0400;     /* File type and mode */
  nlink_t   nlink = 1;       /* Number of hard links */
  uid_t     uid = 0;         /* User ID of owner */
  gid_t     gid = 0;         /* Group ID of owner */
  dev_t     rdev = 0;        /* Device ID (if special file) */
  off_t     size = 0;        /* Total size, in bytes */
  blksize_t blksize = 0;     /* Block size for filesystem I/O */
  blkcnt_t  blocks = 0;      /* Number of 512B blocks allocated */

  time_t atime = 0; /* Time of last access */
  time_t mtime = 0; /* Time of last modification */
  time_t ctime = 0; /* Time of last status change */
};
void copy(struct stat& lhs, FileStatus const& rhs);
