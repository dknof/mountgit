#include "constants.h"
#include "time.h"

Year::Year(int const year) :
  year_(year)
{ }

Year::Year(string const year) :
  year_(stoi(year))
{ }

Year::operator int() const
{ return year_; }

Month::Month(int const month) :
  month_(month)
{
  assert(month_ >= 1 && month_ <= 12);
}

Month::Month(string const month)
{
  // special handling, because 08 and 09 normally are undefined octal values
  if (month == "08")
    month_ = 8;
  else if (month == "09")
    month_ = 9;
  else
    month_ = stoi(month);
  assert(month_ >= 1 && month_ <= 12);
}

Month::operator int() const
{ return month_; }

Day::Day(int const day) :
  day_(day)
{
  assert(day_ >= 1 && day_ <= 31);
}

Day::Day(string const day)
{
  // special handling, because 08 and 09 normally are undefined octal values
  if (day == "08")
    day_ = 8;
  else if (day == "09")
    day_ = 9;
  else
    day_ = stoi(day);
  assert(day_ >= 1 && day_ <= 31);
}

Day::operator int() const
{ return day_; }

Time::Time() :
  Time(Clock::now())
{ }

Time::Time(Clock::time_point time) :
  Clock::time_point(time)
{ }

Time::Time(time_t time) :
  Clock::time_point(Clock::from_time_t(time))
{ }

Time::Time(git_time time) :
  Clock::time_point(Clock::from_time_t(time.time) + std::chrono::minutes(time.offset))
{ }

Time::operator time_t() const
{
  return Clock::to_time_t(*this);
}

Time::operator git_time() const
{
  return {Clock::to_time_t(*this), 0};
}

auto Time::year() const
-> Year
{
  time_t const tt = *this;
  return Year(1900 + localtime(&tt)->tm_year);
}

auto Time::month() const
-> Month
{
  time_t const tt = *this;
  return Month(1 + localtime(&tt)->tm_mon);
}

auto Time::day() const
-> Day
{
  time_t const tt = *this;
  return Day(localtime(&tt)->tm_mday);
}

auto to_string(Time const time)
  -> string
{
  time_t tt = time;
  string text = ctime(&tt);
  if (text.back() == '\n')
    text.pop_back();
  return text;
}

auto to_string(Year const year)
  -> string
{
  return std::to_string(static_cast<int>(year));
}

auto to_string(Month const month)
  -> string
{
  if (month < 10)
    return "0"s + std::to_string(static_cast<int>(month));
  return std::to_string(static_cast<int>(month));
}

auto to_string(Day const day)
  -> string
{
  if (day < 10)
    return "0"s + std::to_string(static_cast<int>(day));
  return std::to_string(static_cast<int>(day));
}
