# mountgit

Filesystem to get a readonly access to a git repository.

## Getting Started

* Get the sourcecode: `git clone https://gitlab.com/dknof/mountgit.git`
* Call `make` to build mountgit
* run `./mountgit <git repository> <mount point>`

### Prerequisites

You need make and a C++-Compiler (with support for C++17).
Libraries used are:

* [libgit2](https://libgit2.org/)
* [libfuse3](https://github.com/libfuse/libfuse)

Debian:

    apt install g++ make
    apt install libgit2-dev libfuse3-dev

### Installing

Just copy the file mountgit into /usr/local/bin or any other directory in your path.

## Running the tests

To run all tests start check\_references.sh from the project directory.
Since the reference uses the repository of mountgit you have to call it from a clone of the full repository.

## Examples

The following examples use the repository of mountgit. To start you have to mount it and change into the mount point:

```
make
mkdir mnt
./mountgit . mnt
cd mnt/
```

### View the ids of all commits
```
ls commits/
```

### View the ids of Sep 2019
```
ls commits-by-month/2019/09
```

### See the author (metadata) of a specific commit
```
cat commits/8ae619bc794fdad0cb34de138b95f8c9d9ec7dcd/author
```

### Search all commits which contains reference in the commit message
```
grep reference commits/*/message
```

### See the id of the parent(s) of a commit
```
ls -l commits/c35323d24f3324f20182830544cf3306f92c2d17/parents
```

### Show in which months of 2019 there was activity (directory size corresponds to the number of commits in that month)
```
ls -l commits-by-month/2019
```

### Show the commits of a specific date
```
ls -l commits-by-day/2019/09/23
```

### Get some metadata of all commits of a day
```
for f in commits-by-day/2019/09/23/*; do cat $f/id $f/time $f/author $f/summary; echo; done
```

### Get the summary of all commits of a specific date
```
cat commits-by-day/2019/09/23/*/summary
```

### Show the id of the parent commit of the master branch
```
cat branches/master/parent/id
```

### Use unionfs to create a read-write-overlay for the filesystem
With an overlay of a rw-filesystem you can simply modify and compile each commit directly. Starting point is on the top level of the project directory.
```
mkdir mnt mnt.git overlay
./mountgit . mnt.git
unionfs -o cow overlay=RW:mnt.git=RO mnt
make -C mnt/commits/c35323d24f3324f20182830544cf3306f92c2d17/files/src
```

## Website

You can get the source code from <https://gitlab.com/dknof/mountgit>.

## Authors

* Dr. Diether Knof <dknof+mountgit@posteo.de>

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

## Status of the project

This project is currently under no active development.
My personal requirements are met. The next steps would be:
* support for tags
* performance
  - cache for the commits
  - reading data from files
If you are interested in improvement feel free to send patches or suggestions to the [author](mailto:dknof+mountgit@posteo.de).
