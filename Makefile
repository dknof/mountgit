.PHONY: all
all: mountgit logo.png

.PHONY: help
help:
	@echo "make           -- create 'mountgit' and the logo and copy it into this directory"
	@echo "make mountgit  -- create 'mountgit' and copy it into this directory"
	@echo "make logo.png  -- create the logo, uses inkscape"
	@echo "make help      -- this help"
	@echo "make test      -- create 'mountgit' and run all tests"
	@echo "make clean     -- delete all temporary files"

.PHONY: mountgit
mountgit:
	$(MAKE) -C src/ mountgit
	cp src/mountgit .

logo.png: logo.svg
	inkscape --export-png=$@ --export-height=64 $<

.PHONY: test
test: mountgit
	./check_references.sh

.PHONY: mountgit.info
mountgit.info:
	lcov --quiet --capture --no-external --directory src --output mountgit.info

.PHONY: coverage-report
coverage-report: mountgit.info
	genhtml --quiet mountgit.info --output-directory coverage-report

.PHONY: clean
clean:
	$(RM) mountgit
	$(RM) mountgit.png
	$(RM) logo.png
	make -C src/ clean
	make -C references/ clean
